from turtle import Turtle


class Ball(Turtle):

    def __init__(self):
        super().__init__()
        self.shape("circle")
        self.penup()
        self.color("white")
        self.shapesize(stretch_wid=1, stretch_len=1)
        # self.goto(position)
        self.y_parameter = 10
        self.x_parameter = 10
        self.move_speed = 0.1

    def move(self):
        new_x = self.xcor() + self.x_parameter
        new_y = self.ycor() + self.y_parameter
        self.goto(new_x, new_y)

    def return_to_starting_position(self):
        self.goto((0, 0))
        self.bounce_x()
        self.move_speed = 0.1

    def bounce_y(self):
        self.y_parameter = -self.y_parameter

    def bounce_x(self):
        self.x_parameter = -self.x_parameter
        self.move_speed *= 0.9
